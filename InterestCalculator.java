public class InterestCalculator {
    private double calculateAmritaROI(int AmritaBalance) {
        int AmritaROI = 3;
        double SI_Till_30_March, SI_On_31_March, totalSI;

        SI_Till_30_March =(double)(AmritaBalance * 89 * AmritaROI)/(365*100);

        AmritaBalance -= 45000;
        SI_On_31_March =(double)(AmritaBalance * AmritaROI)/(365*100);

        totalSI = SI_Till_30_March + SI_On_31_March;
        return totalSI;
    }

    private boolean canGopalOverdraft(int GopalBalance, int GopalOverdraftAmount) {
        if(GopalBalance <= GopalOverdraftAmount) {
            return false;
        }
        return (GopalOverdraftAmount == (0.2 * GopalBalance));
    }

    public static void main(String[] args) {        InterestCalculator bharatBank = new InterestCalculator();
        int GopalBalance = 60000, AmritaBalance = 100000;
        double AmritaInterest = bharatBank.calculateAmritaROI(AmritaBalance);

        System.out.println("Gopal's Interest = 0");
        System.out.println("Amrita's Interest = " + String.format("%.2f", AmritaInterest));

        System.out.println("------------------------------");

        int GopalOverdraftAmount = 70000;
        System.out.println(bharatBank.canGopalOverdraft(GopalBalance, GopalOverdraftAmount) ? "Gopal can withdraw Rs. " + GopalOverdraftAmount : "Gopal cannot withdraw Rs. " + GopalOverdraftAmount);
    }
}
