# Calculate Interest Problem

In this problem, we will find out the interest
of a quarter of 2 customers of Bharat Bank - 
Amrita and Gopal. 

### Calculate Rate of Interest:
- Amrita has a savings account (R.O.I = 3% p.a.)
- Gopal has a current account (R.O.I = 0% p.a.)

### Find if Gopal can overdraft:
We also define if Gopal can withdraw money
from his account. (Limit = 20% of the account balance).

**_ Note: This problem was done as a part of ThoughtWorks + Prograd training. _**